For the application to work correctly, add your files: 51Degrees-EnterpriseV3.4.trie and Enterprise-HashV41.hash to the project folder

To run the jar file (51DegreesPerformance-1.0-SNAPSHOT-jar-with-dependencies.jar),
it is necessary that in the same folder there are 51Degrees-EnterpriseV3.4.trie Enterprise-HashV41.hash UserAgentPerformanceFile.csv files.
Or you can pass the full path to the files as input arguments (path to 51Degrees-EnterpriseV3.4.trie first, path to Enterprise-HashV41.hash second).


Performance results obtained by our team: 
```
Beginning HASH performance checks
Processed 50,000 devices in: 6239 ms
Processed 50,000 devices in: 6038 ms
Processed 50,000 devices in: 5312 ms
Processed 50,000 devices in: 5307 ms
Processed 50,000 devices in: 5370 ms
Processed 50,000 devices in: 5402 ms
Processed 50,000 devices in: 5423 ms
Processed 50,000 devices in: 5425 ms
Processed 50,000 devices in: 5430 ms
Processed 50,000 devices in: 5367 ms


Beginning TRIE performance checks
Processed 50,000 devices in: 797 ms
Processed 50,000 devices in: 1136 ms
Processed 50,000 devices in: 971 ms
Processed 50,000 devices in: 986 ms
Processed 50,000 devices in: 928 ms
Processed 50,000 devices in: 901 ms
Processed 50,000 devices in: 1014 ms
Processed 50,000 devices in: 931 ms
Processed 50,000 devices in: 834 ms
Processed 50,000 devices in: 953 ms
```

