package ly.persona.FiftyOneDegreesPerformance;

import FiftyOneDegreesTrieV3.LibLoader;
import FiftyOneDegreesTrieV3.Match;
import FiftyOneDegreesTrieV3.Provider;
import FiftyOneDegreesTrieV3.VectorString;
import ly.persona.FiftyOneDegreesPerformance.data.Device;
import ly.persona.FiftyOneDegreesPerformance.data.UserAgentDetails;
import fiftyone.devicedetection.DeviceDetectionPipelineBuilder;
import fiftyone.devicedetection.shared.DeviceData;
import fiftyone.pipeline.core.data.FlowData;
import fiftyone.pipeline.core.flowelements.Pipeline;
import fiftyone.pipeline.engines.Constants;
import fiftyone.pipeline.engines.data.AspectPropertyValue;
import fiftyone.pipeline.engines.exceptions.NoValueException;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws Exception {
        String fileTrie = "51Degrees-EnterpriseV3.4.trie";
        String fileHash = "Enterprise-HashV41.hash";

        if (args.length == 2) {
            fileTrie = args[0] != null ? args[0] : fileTrie;
            fileHash = args[1] != null ? args[1] : fileHash;
        }
        List<Device> deviceList = Files.lines(new File("UserAgentPerformanceFile.csv").toPath(), Charset.defaultCharset())
                .limit(50000).map(s -> {
                    Device device = new Device();
                    device.setUa(decodeUrl(s));
                    return device;
                })
                .collect(Collectors.toList());

        Pipeline uaDetailsPipelineHash = new DeviceDetectionPipelineBuilder()
                .useOnPremise(fileHash, false)
                .setDataFileSystemWatcher(false)
                .setShareUsage(false)
                .setPerformanceProfile(Constants.PerformanceProfiles.MaxPerformance)
                .setUsePredictiveGraph(true)
                .setUsePerformanceGraph(false)
                .build();

        LibLoader.load("/FiftyOneDegreesTrieV3.so");
        Provider uaDetailsProviderTrie = new Provider(fileTrie);

        new Example().run(deviceList, uaDetailsPipelineHash);
        new Example().run(deviceList, uaDetailsProviderTrie);
        System.out.println("Done");
    }

    public static class Example {

        public void run(List<Device> deviceList, Pipeline uaDetailsPipelineHash) throws Exception {
            System.out.println("Beginning HASH performance checks");
            int N = 10;
            for (int i = 0; i < N; i++) {
                long start = System.currentTimeMillis();
                for (Device device : deviceList) {
                    getDetailsHash(device, uaDetailsPipelineHash);
                }
                long end = System.currentTimeMillis();
                System.out.println("Processed 50,000 devices in: " + (end - start) + " ms");
            }
        }

        public void run(List<Device> deviceList, Provider uaDetailsPipelineTrie) {
            System.out.println("Beginning TRIE performance checks");
            int N = 10;
            for (int i = 0; i < N; i++) {
                long start = System.currentTimeMillis();
                for (Device device : deviceList) {
                    getDetailsTrie(device, uaDetailsPipelineTrie);
                }
                long end = System.currentTimeMillis();
                System.out.println("Processed 50,000 devices in: " + (end - start) + " ms");
            }
        }

    }

    private static UserAgentDetails getDetailsTrie(Device device, Provider provider) {
        if (device == null) {
            return UserAgentDetails.EMPTY;
        }
        String userAgent = device.getUa();
        UserAgentDetails userAgentDetails = null;

        String iosBuildId = getIosBuildId(userAgent);

        try {
            Match match = provider.getMatch(userAgent);
            if (match != null) {
                Integer screenPixelsWidth = getValueTrie(match, "ScreenPixelsWidth", Integer.class);
                Integer screenPixelsHeight = getValueTrie(match, "ScreenPixelsHeight", Integer.class);
                Double screenInchesDiagonal = getValueTrie(match, "ScreenInchesDiagonal", Double.class);
                userAgentDetails = new UserAgentDetails(
                        getValueTrie(match, "HardwareVendor"),
                        getValueTrie(match, "HardwareModel"),
                        getValueTrie(match, "DeviceType"),
                        getValueTrie(match, "BatteryCapacity", Integer.class),
                        getValueTrie(match, "DeviceRAM", Integer.class),
                        getValueTrie(match, "ReleaseYear", Integer.class),
                        getValueTrie(match, "MaxInternalStorage", Double.class),
                        screenPixelsWidth,
                        screenPixelsHeight,
                        screenInchesDiagonal,
                        calculatePPI(screenPixelsWidth, screenPixelsHeight, screenInchesDiagonal),
                        iosBuildId,
                        getValueTrie(match, "PriceBand"),
                        getValueTrie(match, "NativePlatform")
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userAgentDetails;
    }

    private static UserAgentDetails getDetailsHash(Device device, Pipeline pipeline) throws Exception {


        if (device == null) {
            return UserAgentDetails.EMPTY;
        }
        String userAgent = device.getUa();
        UserAgentDetails userAgentDetails = null;

        String iosBuildId = getIosBuildId(userAgent);

        try (FlowData data = pipeline.createFlowData()) {
            data.addEvidence("header.user-agent", userAgent).process();
            DeviceData deviceData = data.get(DeviceData.class);
            if (deviceData != null) {
                Integer screenPixelsWidth = getValueHash(deviceData.getScreenPixelsWidth());
                Integer screenPixelsHeight = getValueHash(deviceData.getScreenPixelsHeight());
                Double screenInchesDiagonal = getValueHash(deviceData.getScreenInchesDiagonal());
                userAgentDetails = new UserAgentDetails(
                        getValueHash(deviceData.getHardwareVendor()),
                        getValueHash(deviceData.getHardwareModel()),
                        getValueHash(deviceData.getDeviceType()),
                        getValueHash(deviceData.getBatteryCapacity()),
                        getValueHash(deviceData.getDeviceRAM()),
                        getValueHash(deviceData.getReleaseYear()),
                        getValueHash(deviceData.getMaxInternalStorage()),
                        screenPixelsWidth,
                        screenPixelsHeight,
                        screenInchesDiagonal,
                        calculatePPI(screenPixelsWidth, screenPixelsHeight, screenInchesDiagonal),
                        iosBuildId,
                        getValueHash(deviceData.getPriceBand()),
                        getValueHash(deviceData.getNativePlatform())
                );
            }
        }
        return userAgentDetails;
    }


    private static <T> T getValueHash(AspectPropertyValue<T> value) {
        try {
            if (value == null || !value.hasValue()) {
                return null;
            }

            String str = value.getValue().toString();
            if (str == null || "Unknown".equals(str) || "NA".equals(str)) {
                return null;
            }
            return value.getValue();

        } catch (NoValueException ignored) {

        }
        return null;
    }

    private static String getIosBuildId(String ua) {
        int index = ua.indexOf(" Mobile/");
        if (index < 0) {
            return null;
        }
        String cutted = ua.substring(index + " Mobile/".length());
        int spaceIndex = cutted.indexOf(' ');
        if (spaceIndex < 0) {
            return cutted;
        }
        return cutted.substring(0, spaceIndex);
    }

    private static String decodeUrl(String url) {
        try {
            while (url.contains("%") || url.contains("+")) {
                final String decodedUrl = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
                if (!Objects.equals(decodedUrl, url)) {
                    url = decodedUrl;
                } else {
                    break;
                }
            }
        } catch (Exception ignored) {
        }

        return url;
    }

    private static String getValueTrie(Match match, String property) {
        return getValueTrie(match, property, String.class);
    }

    private static <T> T getValueTrie(Match match, String property, Class<T> klass) {
        VectorString values = match.getValues(property);
        if (values.size() > 0) {
            String value = values.get(0);
            try {
                if (value == null || "Unknown".equals(value) || "NA".equals(value)) {
                    return null;
                } else if (klass == String.class) {
                    return klass.cast(value);
                } else if (klass == Double.class) {
                    return klass.cast(Double.parseDouble(value));
                } else if (klass == Integer.class) {
                    return klass.cast(Integer.parseInt(value));
                }
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    static Double calculatePPI(Integer screenPixelsWidth, Integer screenPixelsHeight, Double screenInchesDiagonal) {
        if (screenPixelsWidth == null || screenPixelsHeight == null || screenInchesDiagonal == null) {
            return null;
        }

        BigDecimal diagonal = new BigDecimal(screenInchesDiagonal);
        if (diagonal.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal pixels = BigDecimal.valueOf(Math.sqrt(screenPixelsWidth * screenPixelsWidth + screenPixelsHeight * screenPixelsHeight));
            if (pixels.compareTo(BigDecimal.ZERO) > 0) {
                return pixels.divide(diagonal, 2, RoundingMode.HALF_UP).doubleValue();
            }
        }

        return null;
    }
}


