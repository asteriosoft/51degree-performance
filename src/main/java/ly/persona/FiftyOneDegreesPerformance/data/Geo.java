package ly.persona.FiftyOneDegreesPerformance.data;

import lombok.Data;

import java.util.Map;


@Data
public class Geo {


    private Float lat;

    private Float lon;

    private Integer type;

    private Integer accuracy;

    private Integer lastfix;

    private Integer ipservice;

    private String country;

    private String region;

    private String regionfips104;

    private String metro;

    private String city;

    private String zip;

    private Integer utcoffset;

    private Map<String, Object> ext;
}
