package ly.persona.FiftyOneDegreesPerformance.data;

import lombok.Data;

import java.util.Map;

@Data
public class Device {

    private String ua;

    private Geo geo;

    private Integer dnt;

    private Integer lmt;

    private String ip;

    private String ipv6;

    private Integer devicetype;

    private String make;

    private String model;

    private String os;

    private String osv;

    private String hwv;

    private Integer h;

    private Integer w;

    private Integer ppi;

    private Float pxratio;

    private Integer js;


    private String flashver;

    private String language;

    private String carrier;

    private String mccmnc;

    private Integer connectiontype;

    private String ifa;

    private String didsha1;

    private String didmd5;

    private String dpidsha1;

    private String dpidmd5;

    private String macsha1;

    private String macmd5;

    private Map<String, Object> ext;
}
