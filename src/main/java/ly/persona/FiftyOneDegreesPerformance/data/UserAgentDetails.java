package ly.persona.FiftyOneDegreesPerformance.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAgentDetails {

    public static final UserAgentDetails EMPTY = new UserAgentDetails();

    private String hardwareVendor;
    private String hardwareModel;

    private String deviceType;
    private Integer batteryCapacity;
    private Integer deviceRAM;
    private Integer releaseYear;
    private Double maxInternalStorage;
    private Integer screenPixelsWidth;
    private Integer screenPixelsHeight;
    private Double screenInchesDiagonal;
    private Double PPI;
    private String iosBuildId;
    private String deviceCost;

    private String nativePlatform;
}
